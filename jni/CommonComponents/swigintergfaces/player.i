%module PlayerWrap
%include "std_string.i"

%{
#include "../include/Player.h"

%}

/* Let's just grab the original header file here */
%include "../include/Player.h"