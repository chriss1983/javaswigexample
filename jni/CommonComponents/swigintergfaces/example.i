/* File : example.i */
%module ExampleWrap
%include "std_string.i"

%{
#include "../include/example.h"
//#include <boost/property_tree/ptree.hpp>
//#include <boost/property_tree/json_parser.hpp>
%}

/* Let's just grab the original header file here */
%include "../include/example.h"
