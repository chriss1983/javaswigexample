
#string(REPLACE "." "_" SECU_BOOST_VER ${SECU_BOOST_VERSION_REQUIRED})

ExternalProject_Add(boost
    #URL http://san01.secusmart.intranet/dev-files/boost/boost_${SECU_BOOST_VER}.tar.gz
    URL https://dl.bintray.com/boostorg/release/1.64.0/source/:boost_1_64_0.tar.gz
    SOURCE_DIR ${CMAKE_BINARY_DIR}/boost_${SECU_BOOST_VER}
    CONFIGURE_COMMAND ./bootstrap.sh ${_ICU_SUPPORT}
    BUILD_IN_SOURCE 1
    BUILD_ALWAYS 0
    BUILD_COMMAND
        ./b2 install
        --prefix=${SECU_BOOST_BINARY_DIR}
        ${_BOOST_ADDITIONAL_CONFIG_OPTIONS}
        ${_BOOST_ADDITIONAL_BUILD_OPTIONS}
        ${_BUILD_WITH}
        -j${SECU_PROCESSOR_COUNT}
    PATCH_COMMAND ${EXECUTE_PATCH_COMMAND}
    INSTALL_COMMAND ""
    )

if(SECU_ENABLE_ICU)
  add_dependencies(boost icu)
endif(SECU_ENABLE_ICU)
