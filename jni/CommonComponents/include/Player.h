/*
 * Player.h
 *
 *  Created on: 09.06.2017
 *      Author: christianrichter
 */

#include <string>
#ifndef JNI_EXAMPLE_PLAYER_H_
#define JNI_EXAMPLE_PLAYER_H_


class Player {
public:
	Player();
	virtual ~Player();

	virtual std::string getAddress();
	virtual void setAddress( std::string address);
    virtual	std::string getFirstName();
	virtual void setFirstName( std::string firstName);
	virtual std::string getHouseNumber();
	virtual void setHouseNumber( std::string houseNumber);
	virtual std::string getLastName();
	virtual void setLastName( std::string lastName);
	virtual std::string getStreet();
	virtual void setStreet( std::string street);

private:
	std::string _first_name;
	std::string _last_name;
	std::string _street;
	std::string _house_number;
	std::string _address;
};

#endif /* JNI_EXAMPLE_PLAYER_H_ */
