/* File : example.h */
#include <iostream>
//#include <boost/property_tree/ptree.hpp>
//#include <boost/property_tree/json_parser.hpp>

class Shape {
public:
  Shape() {
	  std::cout << "Hello from Shape" << std:: endl;
	  x = 0;
	  y = 0;
  }
  virtual ~Shape() {
  };
  double  x, y;   
  void    move(double dx, double dy);
  virtual double area() = 0;
  virtual double perimeter() = 0;
  void openFile();
  void printValue(std::string val);

  // Create a root
//  boost::property_tree::ptree root;

};

class Circle : public Shape {
private:
  double radius;
public:
  Circle(double r) : radius(r) {
	  std::cout << "Hello from Circle" << std:: endl;
  };
  virtual double area();
  virtual double perimeter();
};

class Square : public Shape {
private:
  double width;
public:
  Square(double w) : width(w) {
	  std::cout << "Hello from Square" << std:: endl;
  };
  virtual double area();
  virtual double perimeter();
};

