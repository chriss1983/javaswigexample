/* File : example.h */
#include <iostream>
#include <string>

class Test {
public:
	Test();
	~Test();

	void printTest();
};

class Util {
public:
	Util();
	~Util();


	virtual std::string getValue();
	virtual void setValue(std::string val);

	virtual Test getTest();
	virtual void setTest(Test t);

private:
	std::string val_;
	Test t_;
};
