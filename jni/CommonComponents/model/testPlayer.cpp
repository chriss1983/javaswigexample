/*
 * Player.cpp
 *
 *  Created on: 09.06.2017
 *      Author: christianrichter
 */

#include "../include/Player.h"

Player::Player() {
	// TODO Auto-generated ructor stub

}

Player::~Player() {
	// TODO Auto-generated destructor stub
}

std::string Player::getAddress() {
	return _address;
}

void Player::setAddress(std::string address) {
	_address = address;
}

std::string Player::getFirstName() {
	return _first_name;
}

void Player::setFirstName(std::string firstName) {
	_first_name = firstName;
}

std::string Player::getHouseNumber() {
	return _house_number;
}

void Player::setHouseNumber(std::string houseNumber) {
	_house_number = houseNumber;
}

std::string Player::getLastName() {
	return _last_name;
}

void Player::setLastName(std::string lastName) {
	_last_name = lastName;
}

std::string Player::getStreet() {
	return _street;
}

void Player::setStreet(std::string street) {
	_street = street;
}
